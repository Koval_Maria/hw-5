package com.deveducation.www.hw5;


public class AList<Integer> {

    private static final int DEFAULT_CAPACITY = 10;
    private int[] array = new int[DEFAULT_CAPACITY];
    private final int ind = 0;
    private int size;
    private int capacity;
    private int[] someArray;

    public AList() {
    }

    public AList(int capacity) {
        this.capacity = capacity;
    }

    public AList(int[] someArray) {
        this.someArray = someArray;
    }


    public int size() {
        int size = 0;
        for (int i = ind; i < array.length; i++) {
            if (array[i] != 0) {
                size++;
            }
        }
        return size;
    }

    public void clear() {
        for (int i = ind; i < array.length; i++) {
            array[i] = 0;
            size = 0;
        }
    }

    public int get(int index) {
        if (index > array.length - 1) {
            throw new IllegalArgumentException("The index is out of range");
        }
        int gotElement = 0;
        for (int i = ind; i < array.length; i++) {
            if (i == index) {
                gotElement = array[i];
            }
        }
        return gotElement;
    }


    public boolean add(int value) {
        boolean added = false;
        for (int i = ind; i < array.length; i++) {
                if (array[i] == 0) {
                    array[i] = value;
                    added = true;
                    i = array.length;
                }if (i == array.length - 1) {
                        resize(array.length * 2);
                }
            }return added;
        }



    public boolean addByIndex(int index, int value) {
        if (index > array.length) {
            resize(index + array.length);
        }
        boolean added = false;
        for (int i = ind; i < array.length; i++) {
            if (i == index && array[i] == 0) {
                array[i] = value;
                added = true;
                break;
            }
            if (i == index && array[i] != 0) {
                for (int j = i; j < array.length; j++) {
                    if (array[j] == 0) {
                        array[j] = value;
                        added = true;
                        break;
                    }
                }
            }

        }
        return added;
    }

    public int[] remove(int number) {
        int[] newArray = new int[array.length];
        for (int i = ind; i < array.length - 1; i++) {
            if (array[i] == number) {
                newArray[i] = 0;
                array[i] = newArray[i];
            } else {
            }

        }
        return array;
    }

    public int[] removeByIndex(int index) {
        if (index > array.length - 1) {
            throw new IllegalArgumentException("The index is out of range");
        }
        int[] newArray = new int[array.length];
        for (int i = ind; i < array.length - 1; i++) {

            if (i == index) {
                newArray[i] = 0;
                array[i] = newArray[i];
            }
        }
        return array;
    }

    public boolean contains(int value) {
        boolean contain = false;
        for (int i = ind; i < array.length; i++) {
            if (array[i] == value) {
                contain = true;
            }
        }
        return contain;

    }

    public boolean set(int index, int value) {
        if (index > array.length - 1) {
            throw new IllegalArgumentException("The index is out of range");
        }
        boolean done = false;
        for (int i = ind; i < array.length; i++) {
            if (i == index) {
                array[i] = value;
                done = true;
            }
        }
        return done;
    }


    private void resize(int newLength) {
        int[] newArray = new int[newLength];
        System.arraycopy(array, 0, newArray, 0, ind);
        array = newArray;
    }

    public void print() {
        String s = "[";
        int length = array.length;
        for (int i = ind; i < length; i++) {
            if (array[i] != 0 && i != length - 1) {
                s += array[i] + ", ";
            } else if (array[i] != 0 && i == length - 1) {
                s += array[i] + "]";
            }
            if (array[i] == 0 && i != length - 1) {
                s += "-, ";
            }
            if (array[i] == 0 && i == length - 1) {
                s += "-]";
            }
        }
        System.out.println(s);
    }


    public int[] toArray() {
        int length = size();
            int [] newArray = new int[length];
        for(int i = ind; i < array.length; i++) {
            if(array[i] == 0) {
                continue;
            }
            newArray[i] = array[i];
            array[i] = newArray[i];

        }
        return newArray;

    }

    public boolean removeAll(int[] ar) {
        boolean removed = false;
        for (int j = 0; j < ar.length; j++) {
            for (int i = ind; i < array.length; i++) {
                if (ar[j] == array[i]) {
                    array[i] = 0;
                    removed = true;
                }
            }
        }
        return removed;
    }
}

