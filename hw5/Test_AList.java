package com.deveducation.www.hw5;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Test_AList {


    static AList aList;
    static int[] myArray = {14, -157, 1, 0, -12, 96, 1201};
    AList<Integer> myAList = new AList<>();


    @BeforeAll
    public static void init() {
        aList = new AList();
        aList = new AList(2);
        aList = new AList(myArray);
    }


    @ParameterizedTest
    @CsvSource(value = {"true:5", "true:-897665", "true: 0", "true:1"}, delimiter = ':')
    void test_add(boolean expected, int actual) {
        Assertions.assertEquals(expected, aList.add(actual));
    }

    @Test
    public void test_size() {
        myAList.add(1);
        int actual = myAList.size();
        int expected = 1;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_clear() {
        myAList.add(1);
        myAList.add(2);
        myAList.add(3);
        myAList.add(4);
        myAList.clear();
        myAList.print();
        int actual = myAList.size();
        int expected = 0;
        Assertions.assertEquals(expected, actual);
    }


    @Test
    public void test_get_rightIndex() {
        myAList.add(2);
        myAList.add(4);
        myAList.set(1, 999);
        int actual = myAList.get(1);
        int expected = 999;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test_get_wrongIndex() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new AList<>().get(10);
        });
    }

    @Test
    public void test_addByIndex() {
        boolean actual = myAList.addByIndex(1, 1000009);
        boolean expected = true;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_addByIndex_NegIndex() {
        boolean actual = myAList.addByIndex(-1, -10);
        boolean expected = false;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_addByIndex_ZeroIndex() {
        boolean actual = myAList.addByIndex(0, 923);
        boolean expected = true;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_remove() {
        myAList.add(9);
        myAList.add(9);
        myAList.add(-7);
        myAList.add(7);
        myAList.print();
        int[] actual = myAList.remove(9);
        int[] expected = {0, 0, -7, 7, 0, 0, 0, 0, 0, 0};
        myAList.print();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    public void test_removeByIndex() {
        myAList.add(9);
        myAList.add(9);
        myAList.add(-7);
        myAList.add(7);
        int[] actual = myAList.removeByIndex(1);
        int[] expected = {9, 0, -7, 7, 0, 0, 0, 0, 0, 0};
        myAList.print();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    public void test_contains_true() {
        myAList.add(9);
        myAList.add(100);
        myAList.add(-7);
        myAList.add(7);
        boolean actual = myAList.contains(-7);
        boolean expected = true;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_contains_trueZero() {
        myAList.add(9);
        myAList.add(100);
        myAList.add(-7);
        myAList.add(7);
        boolean actual = myAList.contains(0);
        boolean expected = true;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_contains_false() {
        myAList.add(9);
        myAList.add(100);
        myAList.add(-7);
        myAList.add(7);
        boolean actual = myAList.contains(99999);
        boolean expected = false;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_set_rightIndex() {
        boolean actual = myAList.set(9, 111);
        boolean expected = true;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test_set_wrongIndex() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new AList<>().set(10, 111);
        });
    }

    @Test
    public void test_print() {
        boolean actual = myAList.set(9, 111);
        boolean expected = true;
        myAList.print();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_toArray() {
        myAList.add(9);
        myAList.add(1012);
        myAList.add(-9364);
        int[] actual = myAList.toArray();
        int[] expected = {9, 1012, -9364};
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    public void test_removeAll() {
        myAList.add(9);
        myAList.add(14);
        myAList.add(1);
        myAList.add(1012);
        myAList.add(-9364);
        boolean actual = myAList.removeAll(myArray);
        boolean expected = true;
        Assertions.assertEquals(expected, actual);
    }

}